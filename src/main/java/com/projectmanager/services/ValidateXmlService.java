package com.projectmanager.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import java.io.File;

@Slf4j
@Service
@AllArgsConstructor
public class ValidateXmlService {

    public void validateXmlBySchema(File xmlFile) throws Exception {

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        val validator = schemaFactory
                .newSchema(getXmlSchema())
                .newValidator();

        validator.validate(new StreamSource(xmlFile));
    }

    private StreamSource getXmlSchema() {
        return new StreamSource(getClass().getResourceAsStream("/project_schema.xsd"));
    }

}
