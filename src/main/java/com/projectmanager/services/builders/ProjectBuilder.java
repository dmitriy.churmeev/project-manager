package com.projectmanager.services.builders;

import com.projectmanager.entities.Project;
import com.projectmanager.entities.Task;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class ProjectBuilder implements DataBuilder<Project> {

    private final TaskBuilder taskBuilder;

    @Override
    public Project build(Element elementProject) {
        val newProject = new Project();
        String projectKey = getContentFromElementByKey(elementProject, "key");
        String projectName = getContentFromElementByKey(elementProject, "summary");

        newProject.setKey(projectKey);
        newProject.setSummary(projectName);

        NodeList nodeTasks = elementProject.getElementsByTagName("tasks");
        val newTasks = buildTasks(nodeTasks, newProject);

        newProject.setTasks(newTasks);

        return newProject;
    }

    private Set<Task> buildTasks(NodeList nodeTasks, Project project) {
        val tasks = new HashSet<Task>();
        for (int i = 0; i < nodeTasks.getLength(); i++) {
            Node taskNode = nodeTasks.item(i);
            if (taskNode.getNodeType() == Node.ELEMENT_NODE) {
                val task = taskBuilder.build((Element) taskNode);
                task.setProject(project);
                tasks.add(task);
            }
        }
        return tasks;
    }
}
