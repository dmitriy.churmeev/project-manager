package com.projectmanager.services.builders;

import com.projectmanager.entities.Task;
import lombok.val;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

@Service
public class TaskBuilder implements DataBuilder<Task> {

    @Override
    public Task build(Element elementTask) {
        val task = new Task();
        String taskKey = getContentFromElementByKey(elementTask, "key");
        String taskDescription = getContentFromElementByKey(elementTask, "description");
        String taskSummary = getContentFromElementByKey(elementTask, "summary");
        task.setDescription(taskDescription);
        task.setKey(taskKey);
        task.setSummary(taskSummary);
        return task;
    }
}
