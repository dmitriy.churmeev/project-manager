package com.projectmanager.services.builders;

import org.w3c.dom.Element;

public interface DataBuilder<T> {

    T build(Element element);

    default String getContentFromElementByKey(Element elementProject, String key) {
        return elementProject.getElementsByTagName(key).item(0).getTextContent();
    }
}
