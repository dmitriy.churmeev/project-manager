package com.projectmanager.services;

import com.projectmanager.entities.Project;
import com.projectmanager.entities.Task;
import com.projectmanager.repositories.ProjectRepository;
import com.projectmanager.repositories.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ProjectTaskManagerService {

    private final ParseElementService parseElementService;
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;

    public List<Task> getAllProjectsTasks() {
        return taskRepository.findAll();
    }

    public List<Project> uploadProjects(MultipartFile multipartFile) throws Exception {
        val file = uploadFile(multipartFile);
        return projectRepository.saveAll(parseElementService.parseAndBuildProjectsFromXml(file));
    }

    private File uploadFile(MultipartFile multipartFile) throws IOException {
        File newFile = new File(multipartFile.getOriginalFilename());
        newFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(newFile);
        fos.write(multipartFile.getBytes());
        fos.close();
        return newFile;
    }
}
