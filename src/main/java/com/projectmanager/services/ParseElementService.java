package com.projectmanager.services;

import com.projectmanager.entities.Project;
import com.projectmanager.exceptions.ProjectTaskException;
import com.projectmanager.services.builders.ProjectBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class ParseElementService {

    private final ValidateXmlService validateXmlService;
    private final ProjectBuilder projectBuilder;

    public List<Project> parseAndBuildProjectsFromXml(File xml) {
        try {
            validateXmlService.validateXmlBySchema(xml);
            val projectsList = new ArrayList<Project>();
            val documentBuilderFactory = DocumentBuilderFactory.newInstance();
            val documentBuilder = documentBuilderFactory.newDocumentBuilder();
            val document = documentBuilder.parse(xml);
            val projectsNodeList = document.getElementsByTagName("project");

            for (int i = 0; i < projectsNodeList.getLength(); i++) {
                val projectNode = projectsNodeList.item(i);
                if (projectNode.getNodeType() == Node.ELEMENT_NODE) {
                    projectsList.add(projectBuilder.build((Element) projectNode));
                }
            }

            return projectsList;
        } catch (Exception e) {
            log.error("Xml file for task project not valid");
            throw new ProjectTaskException(e);
        }

    }

}
