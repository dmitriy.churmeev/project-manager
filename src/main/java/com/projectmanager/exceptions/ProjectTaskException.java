package com.projectmanager.exceptions;

public class ProjectTaskException extends RuntimeException {

    public ProjectTaskException() {
    }

    public ProjectTaskException(Throwable cause) {
        super(cause);
    }
}
