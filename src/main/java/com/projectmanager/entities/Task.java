package com.projectmanager.entities;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @Column(name = "key")
    private String key;

    @Column(name = "description")
    private String description;

    @ManyToOne(optional = false)
    private Project project;

    public Task(String key, String description, Project project) {
        this.key = project.getKey() + key;
        this.description = description;
        this.project = project;
    }

    public Task() {
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
