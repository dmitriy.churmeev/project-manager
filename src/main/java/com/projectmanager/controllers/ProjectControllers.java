package com.projectmanager.controllers;

import com.projectmanager.entities.Project;
import com.projectmanager.entities.Task;
import com.projectmanager.services.ProjectTaskManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/projects")
public class ProjectControllers {

    private final ProjectTaskManagerService projectTaskManagerService;

    @PostMapping("/load")
    public List<Project> uploadNewProjects(@RequestParam("file") MultipartFile file) throws Exception {
        return projectTaskManagerService.uploadProjects(file);
    }

    @GetMapping
    public List<Task> getAllProjectsTasks() {
        return projectTaskManagerService.getAllProjectsTasks();
    }
}
