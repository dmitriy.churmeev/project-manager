$(document).ready( function () {
	 var table = $('#taskTable').DataTable({
			"sAjaxSource": "http://10.5.0.5:8080/projects",
			"sAjaxDataProp": "",
			"order": [[ 0, "asc" ]],
			"aoColumns": [
			    { "mData": "project.key"},
		        { "mData": "key" },
				{ "mData": "description" }
			]
	 })
});