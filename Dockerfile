FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/project-manager-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} project-manager-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/project-manager-0.0.1-SNAPSHOT.jar"]

